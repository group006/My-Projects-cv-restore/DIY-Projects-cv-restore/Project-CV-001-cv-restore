@echo off

@echo.
@echo %date% %time% >> c:\pd_snapshot.log
@echo %date% %time% PROBLEMATIC DATA SNAPSHOT RUNNING >> c:\pd_snapshot.log
@echo %date% %time% SOURCE PATH:[%1] >> c:\pd_snapshot.log
@echo %date% %time% OUTPUT FILE:[%2] >> c:\pd_snapshot.log

If "%1"=="" GOTO ERROR
If "%2"=="" GOTO ERROR

rem clear all local variables
set globalerror=

rem clear skip variables
set skip_attributes=
set skip_compressed=
set skip_directory_structure=
set skip_encrypted=
set skip_extended_attributes=
set skip_created_times=c
set skip_permissions=
set skip_accessed_times=a
set skip_modified_times=m
set skip_hard_links=
set skip_hidden=
set skip_read_only=
set skip_reparse_point=
set skip_share=
set skip_sparse=
set skip_sums=
set skip_system=
set pdexitshell=
set skip_junctions=

rem set skip variables
:shiftloop
if %3. EQU . goto start
if %3 EQU skip_attributes set skip_attributes=1
if %3 EQU skip_compressed set skip_compressed=1
if %3 EQU skip_directory_structure set skip_directory_structure=1
if %3 EQU skip_encrypted set skip_encrypted=1
if %3 EQU skip_extended_attributes set skip_extended_attributes=1
if %3 EQU skip_created_times set skip_created_times=1
if %3 EQU skip_permissions set skip_permissions=1
if %3 EQU skip_accessed_times set skip_accessed_times=1
if %3 EQU skip_modified_times set skip_modified_times=1
if %3 EQU skip_hard_links set skip_hard_links=1
if %3 EQU skip_hidden set skip_hidden=1
if %3 EQU skip_read_only set skip_read_only=1
if %3 EQU skip_reparse_point set skip_reparse_point=1
if %3 EQU skip_share set skip_share=1
if %3 EQU skip_sparse set skip_sparse=1
if %3 EQU skip_sums set skip_sums=1
if %3 EQU skip_system set skip_system=1
if %3 EQU skip_junctions set skip_junctions=1
if %3 EQU exitshell set pdexitshell=1
shift /3
goto shiftloop

:start
rem Deleting a substituted Directory
rem -------------------------------
rem @echo Deleting a substituted directory
rem subst /D B:

rem sleep 10

rem Substituting the problematic data created.
rem ------------------------------------------
@echo Creating a substituted directory
subst B: %1
if %errorlevel%. NEQ 0. (
set /A globalerror=globalerror+1
goto error )
rem sleep 10

if %skip_directory_structure%. EQU 1. (
@echo *SKIPPING Directory structure
) else (
@echo ---------------------------- > %2
@echo Checking Directory structure of files >> %2
@echo ---------------------------- >> %2
@echo Checking Directory structure of files
REM cscript //NOLOGO b:\bin\deep_paths.vbs b:\ display >> %2
if %skip_created_times%. EQU c. (
@echo   including create time checking
) else (
@echo   excluding create time checking
)
if %skip_accessed_times%. EQU a. (
@echo   including access time checking 
) else (
@echo   excluding access time checking 
)
if %skip_modified_times%. EQU m. (
@echo   including mod time checking 
) else (
@echo   excluding mod time checking 
)
cscript //NOLOGO b:\bin\deep_paths.vbs b:\ display %skip_created_times%%skip_accessed_times%%skip_modified_times%  >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_hidden%. EQU 1. (
@echo *SKIPPING Hidden files
) else (
@echo ------------ >> %2
@echo Checking Hidden files  >> %2
@echo ------------ >> %2
@echo Checking Hidden files 
dir B:\ /s /b /AH | findstr /v /i /c:"bytes free" >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_system%. EQU 1. (
@echo *SKIPPING System files
) else (
@echo ------------- >> %2
@echo Checking System files >> %2
@echo ------------- >> %2
@echo Checking System files
dir B:\ /s /b /AS | findstr /v /i /c:"bytes free" >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_read_only%. EQU 1. (
@echo *SKIPPING Read Only files
) else (
@echo --------------- >> %2
@echo Checking Read Only files >> %2
@echo --------------- >> %2
@echo Checking Read Only files
dir B:\ /s /b /AR | findstr /v /i /c:"bytes free" >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

REM if %skip_created_times%. EQU 1. (
REM @echo *SKIPPING File Created times
REM ) else (
REM @echo ------------------ >> %2
REM @echo Checking File Created times >> %2
REM @echo ------------------ >> %2
REM @echo Checking File created times
REM dir B:\ /s /tc | findstr /v /i /c:"bytes free" >> %2
REM @echo. >> %2
REM @echo. >> %2
REM @echo. >> %2
REM @echo. >> %2
REM rem sleep 10
REM )

REM if %skip_accessed_times%. EQU 1. (
REM @echo *SKIPPING File Accessed times
REM ) else (
REM @echo -------------------- >> %2
REM @echo Checking File Accessed times >> %2
REM @echo -------------------- >> %2
REM @echo Checking File Accessed times
REM dir B:\ /s /ta | findstr /v /i /c:"bytes free" >> %2
REM @echo. >> %2
REM @echo. >> %2
REM @echo. >> %2
REM @echo. >> %2
REM rem sleep 10
REM )

REM if %skip_modified_times%. EQU 1. (
REM @echo *SKIPPING File Modified times
REM ) else (
REM @echo -------------------- >> %2
REM @echo Checking File Modified times >> %2
REM @echo -------------------- >> %2
REM @echo Checking File Modified times
REM dir B:\ /s /tw | findstr /v /i /c:"bytes free" >> %2
REM @echo. >> %2
REM @echo. >> %2
REM @echo. >> %2
REM @echo. >> %2
REM rem sleep 10
REM )

if %skip_attributes%. EQU 1. (
@echo *SKIPPING File Attributes
) else (
@echo ------------------ >> %2
@echo Checking File Attributes>> %2
@echo ------------------ >> %2
@echo Checking File Attributes 
attrib b:\* /s >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_extended_attributes%. EQU 1. (
@echo *SKIPPING File Extended Attributes
) else (
@echo ---------------- >> %2
@echo Checking File Extended Attributes >> %2
@echo ---------------- >> %2
@echo Checking File Extended Attributes
B:\bin\filetest_%PROCESSOR_ARCHITECTURE%.exe -getea "b:\Attribute files\extended\extended_attr_with content.txt" >> %2
B:\bin\filetest_%PROCESSOR_ARCHITECTURE%.exe -getea "b:\Attribute files\extended\extended_attr_NO content.txt" >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_compressed%. EQU 1. (
@echo *SKIPPING Compressed files
) else (
@echo ---------------- >> %2
@echo Checking Compressed files >> %2
@echo ---------------- >> %2
@echo Checking Compressed files
compact .\*.* >> %2
compact /s "B:\Compressed data\*.*"  >> %2
compact /s "B:\Encrypted file\*.*"  >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_encrypted%. EQU 1. (
@echo *SKIPPING Encrypted files
) else (
@echo ---------------- >> %2
@echo Checking Encrypted files >> %2
@echo ---------------- >> %2
@echo Checking Encrypted files
cipher B:\*.* >> %2
cipher "B:\Compressed data\*.*"  >> %2
cipher "B:\Encrypted file\*.*"  >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_sums%. EQU 1. (
@echo *SKIPPING SUMs of files
) else (
@echo ------------ >> %2
@echo Checking SUMs of files >> %2
@echo ------------ >> %2
@echo Checking SUMs of files [may take a while]
b:\bin\fciv.exe b:\ -r -both -exc b:\exclude.txt | find /i ":\" >> %2
for /l %%a in (1,1,256) do b:\bin\fciv.exe -both "b:\Files with streams\stream_file.txt:stream_%%a.txt" | find /i "stream" >> %2
rem fci will output a few errors on some files [thats OK, we get sums on most all files]
rem delete the error output file so that it wont cause a differance later on
del /q b:\fciv.err
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_sparse%. EQU 1. (
@echo *SKIPPING Sparse file
) else (
@echo ----------------- >> %2
@echo Checking Sparse file >> %2
@echo ----------------- >> %2
@echo Checking Sparse file
@echo B:\sparse file\sparesefile.txt >> %2
fsutil sparse queryflag "B:\sparse file\sparsefile.txt" >> %2
fsutil sparse queryrange "B:\sparse file\sparsefile.txt" >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

rem if %skip_reparse_point%. EQU 1. (
rem @echo *SKIPPING Reparse point
rem ) else (
rem @echo ------------------- >> %2
rem @echo Checking Reparse point >> %2
rem @echo ------------------- >> %2
rem @echo Checking Reparse point
rem @echo B:\Reparse point\reparsefile.txt >> %2
rem fsutil reparsepoint query "B:\Reparse point\reparsefile.txt" >> %2
rem @echo. >> %2
rem @echo. >> %2
rem @echo. >> %2
rem @echo. >> %2
rem sleep 10
rem )

if %skip_junctions%. EQU 1. (
@echo *SKIPPING Junctions
) else (
@echo ---------------- >> %2
@echo Checking Junctions >> %2
@echo ---------------- >> %2
b:\bin\junction.exe -s "b:\junction files" >> %2 
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_hard_links%. EQU 1. (
@echo *SKIPPING Hard Links
) else (
@echo ---------------- >> %2
@echo Checking Hard Links >> %2
@echo ---------------- >> %2
c:
cd \
b:\bin\hlscan.exe /dir "b:\Hard link" | findstr /v /i /c:"time" /c:"id:" >> %2
del /q hlscan.err
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_permissions%. EQU 1. (
@echo *SKIPPING File permissions
) else (
@echo ---------------- >> %2
@echo Checking File permissions >> %2
@echo ---------------- >> %2
@echo Checking File permissions
cacls "B:\File permissions\*.*" >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
@echo. >> %2
rem sleep 10
)

if %skip_share%. EQU 1. (
@echo *SKIPPING Share information
) else (
@echo ---------------- >> %2
@echo Checking Share information >> %2
@echo ---------------- >> %2
@echo Checking Share information
net view \\%computername% >> %2
)

rem Cleanup
@echo Deleting the substituted volume
subst /D B:

GOTO END

:ERROR

@echo.
@echo File name:     snapshot.bat
@echo Description:   This file creates a snapshot for verification of problematic data
@echo.
@echo Usage:         snapshot.bat   "source_path" "snapshot_file" 
@echo                [skip_attributes] [skip_compressed] [skip_directory_structure] [skip_encrypted] 
@echo                [skip_extended_attributes] [skip_created_times] [skip_permissions] [skip_accessed_times]
@echo                [skip_modified_times] [skip_hard_links] [skip_hidden] [skip_read_only] 
@echo                [skip_reparse_point] [skip_share] [skip_sparse] [skip_sums] [skip_system]
@echo.
@echo                source_path:   top level folder to begin the snapshot
@echo                snapshot_file: output file of snapshot
@echo                optional skip_parameters: include none or as many skip parameters (delimited by space)
@echo                 as desired to skip sections of the snapshot.
@echo.
@echo EXAMPLE1:      snapshot.bat C:\pd_dst C:\snap_source.txt
@echo EXAMPLE2:      snapshot.bat C:\pd_dst C:\snap_source.txt skip_accessed_times skip_hard_links
@echo.

:END
@echo %date% %time% PROBLEMATIC DATA SNAPSHOT FINISHED with error code[%globalerror%] >> c:\pd_snapshot.log
@echo.
@echo.
@echo.

rem if running through automation exit right away, if not exit the bat and leave the shell open

if %pdexitshell%. EQU 1. (
exit %globalerror%
) else (
exit /B %globalerror%
)
