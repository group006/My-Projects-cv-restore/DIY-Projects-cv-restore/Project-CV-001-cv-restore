

HOW TO USE THE PROBLAMATIC DATA SET FOR WINDOWS FILESYSTEM


1. Run Setup.bat to copy the data set to your computer.
1a.Setup MUST be run from a command prompt with the current working directory where setup.bat resides on \\vega

NOTE: the default install location is [driveletter]:\pd_dst
Provide the drive to install to only.

Example: X:> setup.bat D:\

1b. After setup completes it should indicate if it thinks the problematic data has been installed correctly or not.
1c. If not, report the problem to David Owen.


2. Run the snapshot.bat to capture a picuture of the data before backup.
Example:  d:\pd_dst> snapshot.bat d:\pd_dst d:\snap_before.txt

3. Backup the data.

4. Restore the data to an out of place location. (d:\pd_rst)
NOTE: in order to make the restore snapshot match the backup snapshot 2 things should be done:
a. Restore to a path having the same root level depth as the source data (i.e: d:\pd_pst=9 chars > restore to d:\pd_rst=9 chars)
b. Be sure to remove 1 strip level from the BEGINNING of the path so as NOT to restore to "d:\pd_rst\pd_dst".

5. Run the snapshot.bat to capture a picture of the data after restore.
Example:  d:\pd_dst> snapshot.bat d:\dp_rst d:\snap_after.txt

6. Compare the capture files (you can use windows command fc.exe)

Example: fc snap_before.txt snap_after.txt
"no differances" should be the result

If there are differances you can use wdiff to see whats wrong.
Example: d:\pd_dst\bin\wdiff d:\snap_before.txt  d:\snap_after.txt

History:

12/16/2009 dowen:
Added support to specify the maximum deep path depth setting an environment variable "maxpath" before running setup.
Example: set maxpath=500

12/03/2009 dowen:
Added support to install to any path you want by using "forcepath" as the second parameter.
Also allowing install paths with spaces so long as they are double qouted.


11/19/2009 dowen:
Added extended attribute files
Hard coded the instal path to [driveletter[:\pd_dst so that it will allways match installs for automation
Added "delete" switch to allow only the deleting data and quitting.
Made it aware of local vs remote drives.  remote drives (filers) will skip things not possible on the filer.


11/13/2009 dowen:
Added deep path and files up to 1000
Changed compressed data to have some compressed and some non compressed files
Changed encrypted data to have some encrypted and some non encrypted files
Moved all "helper" programs under \bin
Update setup.bat to clear off the pre-existing problematic data if its found in the same location as being added to
Added shared folder that is shared 1000 times (TR)
Enhanced setup.bat to compare what was written to a gold standard to help make sure that what was written it what is expected.

