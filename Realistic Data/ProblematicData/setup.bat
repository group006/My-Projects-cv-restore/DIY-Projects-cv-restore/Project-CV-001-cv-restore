@echo off
@echo.

SETLOCAL
SET curpath=%3
@echo %date% %time% >> "%3\pd.log"
@echo %date% %time% PROBLEMATIC DATA SETUP RUNNING: [%0] [%1] [%2] [%3] [%4] >> "%3\pd.log"

rem ----------------------------------------------------
rem Check environment
rem ----------------------------------------------------

If %1. EQU . GOTO USAGEERROR

rem clear all local variables
set globalerror=
set step=
set pdpath=
set pddrive=
set pddriveTYPE=
set pdexitshell=

rem initialize variables
set globalerror=0
set step=0

:shiftloop
if %2. EQU . goto start
if %2 EQU delete set pd_delete=1
if %2 EQU forcepath set pd_forcepath=1
if %2 EQU exitshell set pdexitshell=1
shift /2
goto shiftloop
:start

set pdpath=%1
rem remove double quotes if present on parameter 1
for /F "usebackq delims==" %%I in (`@echo %pdpath%`) do set pdpath=%%~I

rem if pd_forcepath is set or pd_delete then allow the install to be whatever is entered
if %pd_forcepath%. NEQ 1. (
rem if not deleting the path FORCE it to be drive:\pd_dst
if %pd_delete%. NEQ 1. set pdpath=%pdpath:~0,1%:\pd_dst
)

rem set the drive to the first character
set pddrive=%pdpath:~0,1%

rem check for UNC path entered, bail if found
if %pddrive%. EQU \. (
@echo Setup does not support a target of UNC path.  
@echo Map a drive to %pdpath% it and run setup again
@echo.
goto USAGEERROR
)

if %pd_delete%. EQU 1. (
rem set pdpath=%1
@echo Deleting: [%pdpath%]
) else (
@echo Installing to: [%pdpath%]
)

rem check if drive given is local or mapped
rem fsutil returns the native language string. Use vbscript program instead.
rem fsutil fsinfo drivetype %pddrive%: |find /i "fixed" > nul
cscript //nologo "%curpath%\bin\drivetype.vbs" %pddrive% | "%curpath%\bin\find" /i "fixed" > nul


if %errorlevel%. EQU 0. (
set pddriveTYPE=LOCAL
) else (
set pddriveTYPE=REMOTE
) 
@echo PD Drivetype: [%pddriveTYPE%]

rem pause

rem ----------------------------------------------------
rem Cleanup the existing problematic data set
rem ----------------------------------------------------
set /A step=step+1
@echo Step %step%: Cleaning up any existing Problematic data
@echo "cleanup started" >> "%pdpath%\started.txt"

if exist "%pdpath%" (
@echo         Deleting junctions...
"%curpath%\bin\junction.exe" -d "%pdpath%\Junction files\junction source"  > nul
@echo         Cleaning up attribute files...
attrib -H "%pdpath%\*.*" /S > nul
attrib -R "%pdpath%\*.*" /S > nul
attrib -S -H "%pdpath%\*.*" /S > nul

REM if %noshare%. EQU 1. (
REM @echo         Deleting old shares...
REM @echo         ...This step is commented out for now
REM rem the find foo at the end of this next line is to supress echoing to the screen only
REM for /l %%i in (1,1,1000) do net share 123456789012345678901234567890123456789012345678901234567890123456789012345_%%i /delete 2>&1 | find "foo" > nul
REM )

@echo         Deleting old problematic data folder.
cscript //Nologo "%curpath%\bin\deep_paths.vbs" "%pdpath%" delete
) else (
@echo         Existing problematic data folder not found
)

if exist "%pdpath%" (
@echo Failed to cleanup existing problematic data
set /A globalerror=globalerror+1
goto error
)
@echo.

if %pd_delete%. EQU 1. goto END

mkdir "%pdpath%"

rem ----------------------------------------------------
rem Copying the problematic data on the client computer
rem ----------------------------------------------------
set /A step=step+1
@echo Step %step%: Copying Problematic data
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo         Creating users to be used later
REM for /l %%i in (1,1,100) do net user TEST_USER_%%i Password12345 /add | .\bin\find "foo" > nul
@echo         Copying files...
xcopy "*" "%pdpath%" /S /E /H /I /Y /C /Q


rem create the snapshot exclusion file
@echo b:\Deep_Paths_upto1000> "%pdpath%\exclude.txt"
@echo.

rem pause

rem --------------------
rem Create Archive files
rem --------------------
set /A step=step+1
@echo Step %step%: Creating an archive file
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
@echo This is an archive file > "%pdpath%\attribute files\archive\archive file.txt"
@echo This is an archive file > "%pdpath%\attribute files\archive\archive file.doc"
attrib +A "%pdpath%\attribute files\hidden\archive file.txt" > nul 
attrib +A "%pdpath%\attribute files\hidden\archive file.doc" > nul 

rem --------------------
rem Create Hidden files
rem --------------------
set /A step=step+1
@echo Step %step%: Creating a hidden file
@@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
@echo This is a hidden file > "%pdpath%\attribute files\hidden\hidden file.txt"
@echo This is a hidden file > "%pdpath%\attribute files\hidden\hidden file.doc"
attrib +H "%pdpath%\attribute files\hidden\hidden file.txt" > nul 
attrib +H "%pdpath%\attribute files\hidden\hidden file.doc" > nul 
rem hidden folders
@echo This is a NON hidden file in a hidden folder > "%pdpath%\attribute files\hidden\hidden_folder_with_files\hidden folder NON hidden file.txt"
@echo This is a NON hidden file in a hidden folder  > "%pdpath%\attribute files\hidden\hidden_folder_with_files\hidden folder NON hidden file.doc"
attrib +H "%pdpath%\attribute files\hidden\hidden_folder_with_files" > nul
attrib +H "%pdpath%\attribute files\hidden\hidden_folder_NO_files" > nul

rem ---------------------
rem Create Readonly files
rem ---------------------
set /A step=step+1
@echo Step %step%: Creating a Readonly Files
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
@echo This is a readonly file > "%pdpath%\attribute files\read only\Readonly file.txt"
@echo This is a readonly file > "%pdpath%\attribute files\read only\Readonly file.doc"
attrib +R "%pdpath%\attribute files\read only\Readonly file.txt" > nul 
attrib +R "%pdpath%\attribute files\read only\Readonly file.doc" > nul 

rem ------------------
rem Create System file
rem ------------------
set /A step=step+1
@echo Step %step%: Creating a System file
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
@echo This is a system file > "%pdpath%\attribute files\system\System file.txt"
@echo This is a system file > "%pdpath%\attribute files\system\System file.doc"
attrib +S "%pdpath%\attribute files\system\System file.txt" > nul 
attrib +S "%pdpath%\attribute files\system\System file.doc" > nul 

rem ------------------
rem Create extended attribute files
rem ------------------
set /A step=step+1
@echo Step %step%: Creating extended attribute files
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
rem Please use \\quark\devl\Tools\FileTestEx\TransactedVersion to create Extended Attribute files.
@echo This file has extended attributes > "%pdpath%\Attribute files\extended\\extended_attr_with content.txt"
"%curpath%\bin\filetest_%PROCESSOR_ARCHITECTURE%.exe" -setea "%pdpath%\Attribute files\extended\extended_attr_with content.txt" attr1 1234567890123456789012345678901234567890
"%curpath%\bin\filetest_%PROCESSOR_ARCHITECTURE%.exe" -setea "%pdpath%\Attribute files\extended\extended_attr_NO content.txt" attr1 1234567890123456789012345678901234567890


rem --------------------------
rem File with double extension
rem --------------------------
set /A step=step+1
@echo Step %step%: Creating file with double extension. *.txt.txt
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
copy "%curpath%\bin\template file.txt" "%pdpath%\files with double extension\file.txt.txt" > nul
copy "%curpath%\bin\template file.txt" "%pdpath%\files with double extension\file.doc.doc" > nul 
@echo This file has a .txt.txt extension > "%pdpath%\files with double extension\file.txt.txt" 
@echo This file has a .doc.doc extension > "%pdpath%\files with double extension\file.doc.doc" 

if %pddriveTYPE%. EQU LOCAL. set /A step=step+1
if %pddriveTYPE%. EQU LOCAL. (
	rem -----------------------
	rem Create compressed files
	rem -----------------------
	@echo Step %step%: Creating compressed files [2 compressed, 2 non compressed]
	@echo "Step %step%" >> "%pdpath%\started.txt"
	@echo.
	copy "%curpath%\bin\template file.txt" "%pdpath%\compressed data\NON_compressed file.txt" > nul 
	copy "%curpath%\bin\template file.txt" "%pdpath%\compressed data\NON_compressed file.doc" > nul 
	@echo This is NOT a compressed file > "%pdpath%\compressed data\NON_compressed file.txt"
	@echo This is NOT a compressed file > "%pdpath%\compressed data\NON_compressed file.doc"
	rem add an encrypted file to the folder to be compressed
	copy "%curpath%\bin\template file.txt" "%pdpath%\compressed data\encrypted file.txt" > nul 
	copy "%curpath%\bin\template file.txt" "%pdpath%\compressed data\encrypted file.doc" > nul 
	@echo This is an encrypted file > "%pdpath%\compressed data\encrypted file.txt"
	@echo This is an encrypted file > "%pdpath%\compressed data\encrypted file.doc"
	if %pddriveTYPE%. EQU LOCAL. cipher /A /E "%pdpath%\compressed data\encrypted file.txt" > nul
	if %pddriveTYPE%. EQU LOCAL. cipher /A /E "%pdpath%\compressed data\encrypted file.doc" > nul
	if %pddriveTYPE%. EQU LOCAL. compact /C "%pdpath%\compressed data" > nul 
	copy "%curpath%\bin\template file.txt" "%pdpath%\compressed data\compressed file.txt" > nul 
	copy "%curpath%\bin\template file.txt" "%pdpath%\compressed data\compressed file.doc" > nul 
	@echo This is a compressed file > "%pdpath%\compressed data\compressed file.txt"
	@echo This is a compressed file > "%pdpath%\compressed data\compressed file.doc"
	copy "%curpath%\bin\pd_LOCAL_baseline_check.txt" "%pdpath%\compressed data\large_compressed file.doc" > nul
) else (
	@echo *** Skipping Compressed files for REMOTE drives
	@echo. 
)

if %pddriveTYPE%. EQU LOCAL. set /A step=step+1
if %pddriveTYPE%. EQU LOCAL. (
rem ----------------------
rem Create Encrypted files
rem ----------------------
@echo Step %step%: Creating Encrypted files [2 encrypted, 2 non encrypted]
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
copy "%curpath%\bin\template file.txt" "%pdpath%\encrypted file\NON_encrypted file.txt" > nul 
copy "%curpath%\bin\template file.txt" "%pdpath%\encrypted file\NON_encrypted file.doc" > nul 
@echo This is an NOT an encrypted file > "%pdpath%\encrypted file\NON_encrypted file.txt"
@echo This is an NOT an encrypted file > "%pdpath%\encrypted file\NON_encrypted file.doc"
rem encrypt the folder, all files added after this should be encrypted
rem add a compressed file to the folder to be encrypted
copy "%curpath%\bin\template file.txt" "%pdpath%\encrypted file\compressed file.txt" > nul 
copy "%curpath%\bin\template file.txt" "%pdpath%\encrypted file\compressed file.doc" > nul 
@echo This is an encrypted file > "%pdpath%\encrypted file\compressed file.txt"
@echo This is an encrypted file > "%pdpath%\encrypted file\compressed file.doc"
if %pddriveTYPE%. EQU LOCAL. compact /C "%pdpath%\encrypted file\compressed file.txt" > nul
if %pddriveTYPE%. EQU LOCAL. compact /C "%pdpath%\encrypted file\compressed file.doc" > nul
if %pddriveTYPE%. EQU LOCAL. cipher /E "%pdpath%\encrypted file" > nul
copy "%curpath%\bin\template file.txt" "%pdpath%\encrypted file\encrypted file.txt" > nul 
copy "%curpath%\bin\template file.txt" "%pdpath%\encrypted file\encrypted file.doc" > nul 
@echo This is an encrypted file > "%pdpath%\encrypted file\encrypted file.txt"
@echo This is an encrypted file > "%pdpath%\encrypted file\encrypted file.doc"
) else (
@echo *** Skipping Encrypted files for REMOTE drives
@echo. 
)

if %pddriveTYPE%. EQU LOCAL. set /A step=step+1
if %pddriveTYPE%. EQU LOCAL. (
:SPARSE
rem -----------
rem Sparse file
rem -----------
@echo Step %step%: Creating Sparse file
@echo.
rem sparse.exe doesnt work on substituted drives use fsutil instead
rem .\bin\sparse.exe "%pdpath%\sparse file\sparsefile.txt" 1 100 12 5 1 > nul 
@echo sparsefile > "%pdpath%\sparse file\sparsefile.txt"
for /l %%i in (1,1,1000) do echo sparsefile >> "%pdpath%\sparse file\sparsefile.txt"
fsutil sparse setflag "%pdpath%\sparse file\sparsefile.txt"
for /l %%i in (10,50,13000) do fsutil sparse setrange "%pdpath%\sparse file\sparsefile.txt" %%i 10
) else (
@echo *** Skipping Sparse files for REMOTE drives
@echo. 
)

rem if %pddriveTYPE%. EQU LOCAL. set /A step=step+1
rem if %pddriveTYPE%. EQU LOCAL. (
rem ----------------------
rem Create Encrypted with Compressed files with ACLS
rem ----------------------
rem @echo Step %step%: Encrypted with Compressed files with ACLS
rem @echo "Step %step%" >> "%pdpath%\started.txt"
rem @echo          This step is commented out for now
rem @echo.
rem ) else (
rem @echo *** Skipping Encrypted with Compressed files with ACLS for REMOTE drives
rem @echo. 
rem )

if %pddriveTYPE%. EQU LOCAL. set /A step=step+1
if %pddriveTYPE%. EQU LOCAL. (
rem ---------
rem Hard link
rem ---------
@echo Step %step%: Creating Hard link files
@echo.
copy "%curpath%\bin\template file.txt" "%pdpath%\hard link\file1.txt" > nul
copy "%curpath%\bin\template file.txt" "%pdpath%\hard link\file1.doc" > nul
@echo This is a source file for hard link > "%pdpath%\hard link\file1.txt"
@echo This is a source file for hard link > "%pdpath%\hard link\file1.doc"
fsutil hardlink create "%pdpath%\hard link\hardlink.txt" "%pdpath%\hard link\file1.txt" > nul 
fsutil hardlink create "%pdpath%\hard link\hardlink.doc" "%pdpath%\hard link\file1.doc" > nul 
) else (
@echo *** Skipping Hard link files for REMOTE drives
@echo.
)

if %pddriveTYPE%. EQU LOCAL. set /A step=step+1
if %pddriveTYPE%. EQU LOCAL. (
rem --------------
rem Junction files
rem --------------
@echo Step %step%: Creating Junction files
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
"%curpath%\bin\junction.exe" "%pdpath%\junction files\junction source" "%pdpath%\junction files\junction target" > nul 
copy "%curpath%\bin\template file.txt" "%pdpath%\junction files\junction source\file1.txt" > nul 
copy "%curpath%\bin\template file.txt" "%pdpath%\junction files\junction source\file1.doc" > nul 
rem circular junction
REM commented out because circular junctions are not currently supported
REM .\bin\junction.exe "%pdpath%\junction files\junction circular" "%pdpath%\junction files" > nul 
) else (
@echo *** Skipping Junctions point files for REMOTE drives
@echo. 
)

rem ----------------------------------
rem Create deep path [up to 255 chars, shell limits]
rem ----------------------------------
set /A step=step+1
@echo Step %step%: Creating deep paths [as far as the shell allows, 255 chars]
cscript //Nologo "%curpath%\bin\deep_paths.vbs" "%pdpath%\Deep_Paths_upto255" add 255 nt > nul

rem @echo Its OK to ignore this message below [The filename or extension is too long]
rem @echo Its OK to ignore this message below [The system cannot find the path specified.]
rem @echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
rem set quitcreatedir=
rem set deepdir=
rem mkdir "%pdpath%\Deep_Paths_upto255" 
rem set deepdir=%pdpath%\Deep_Paths_upto255
rem for /L %%i in (1,1,100) do call :createdir %%i 
rem goto DEEPER_PATH

rem :createdir
rem if %quitcreatedir%. EQU 1. goto skipcreatedir
rem set deepdir=%deepdir%\%1
rem mkdir %deepdir% > nul
rem if %errorlevel%. EQU 1. set quitcreatedir=1
rem @echo %deepdir%\deep_path_file_%1 > "%deepdir%\%1.txt"
rem :skipcreatedir
rem goto :EOF

:DEEPER_PATH
if %maxpath%. EQU . set maxpath=1000
rem ----------------------------------
rem Create deep path [up to %maxpath% chars]
rem ----------------------------------
set /A step=step+1
@echo Step %step%: Creating deep paths [up to %maxpath% chars]
@echo "Step %step%" >> "%pdpath%\started.txt"
cscript //Nologo "%curpath%\bin\deep_paths.vbs" "%pdpath%\Deep_Paths_upto%maxpath%" add %maxpath% win32 > nul
@echo.

rem ----------------
rem File permissions
rem ----------------
set /A step=step+1
@echo Step %step%: Creating files with different Permission for user administrator and system account
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.

rem md "%pdpath%\File permissions"
rem NO RIGHTS
@echo This file has denied permission for administrator > "%pdpath%\File permissions\no rights_administrator_account.txt" 
@echo This file has denied permission for system > "%pdpath%\File permissions\no rights_system_account.doc" 
echo Y| cacls "%pdpath%\File permissions\no rights_administrator_account.txt" /D administrator > nul 
echo Y| cacls "%pdpath%\File permissions\no rights_system_account.doc" /D system > nul 
rem pause

rem FULL RIGHTS
@echo This file has full rights for administrator> "%pdpath%\File permissions\full rights_administrator_account.txt"
@echo This file has full rights for system > "%pdpath%\File permissions\full rights_system_account.doc"
echo Y| cacls "%pdpath%\File permissions\full rights_administrator_account.txt" /D administrator > nul 
echo Y| cacls "%pdpath%\File permissions\full rights_system_account.doc" /D system > nul 
echo Y| cacls "%pdpath%\File permissions\full rights_administrator_account.txt" /E /G administrator:F  > nul 
echo Y| cacls "%pdpath%\File permissions\full rights_system_account.doc" /E /G system:F  > nul 
rem pause

rem READ ONLY
@echo This file has readonly rights for administrator > "%pdpath%\File permissions\readonly rights_administrator_account.txt"
@echo This file has readonly rights for system > "%pdpath%\File permissions\readonly rights_system_account.doc"
echo Y| cacls "%pdpath%\File permissions\readonly rights_administrator_account.txt" /D administrator  > nul 
echo Y| cacls "%pdpath%\File permissions\readonly rights_system_account.doc" /D system  > nul 
echo Y| cacls "%pdpath%\File permissions\readonly rights_administrator_account.txt" /E /G administrator:R > nul 
echo Y| cacls "%pdpath%\File permissions\readonly rights_system_account.doc" /E /G system:R  > nul 
rem pause

rem WRITE ONLY
@echo This file has write only rights for administrator > "%pdpath%\File permissions\write only rights_administrator_account.txt"
@echo This file has write only rights for system > "%pdpath%\File permissions\write only rights_system_account.doc"
echo Y| cacls "%pdpath%\File permissions\write only rights_administrator_account.txt" /D administrator > nul 
echo Y| cacls "%pdpath%\File permissions\write only rights_system_account.doc" /D system  > nul 
echo Y| cacls "%pdpath%\File permissions\write only rights_administrator_account.txt" /E /G administrator:W  > nul 
echo Y| cacls "%pdpath%\File permissions\write only rights_system_account.doc" /E /G system:W > nul 
rem pause

rem READ and WRITE
@echo This file has read and write rights for administrator > "%pdpath%\File permissions\read and write rights_administrator_account.txt"
@echo This file has read and write rights for system > "%pdpath%\File permissions\read and write rights_system_account.doc" 
echo Y| cacls "%pdpath%\File permissions\read and write rights_administrator_account.txt" /D administrator > nul 
echo Y| cacls "%pdpath%\File permissions\read and write rights_system_account.doc" /D system > nul 
echo Y| cacls "%pdpath%\File permissions\read and write rights_administrator_account.txt" /E /G administrator:R > nul 
echo Y| cacls "%pdpath%\File permissions\read and write rights_system_account.doc" /E /G system:R > nul 
echo Y| cacls "%pdpath%\File permissions\read and write rights_administrator_account.txt" /E /G administrator:W > nul 
echo Y| cacls "%pdpath%\File permissions\read and write rights_system_account.doc" /E /G system:W > nul 
rem pause

rem CHANGE ONLY
@echo This file has change rights only for administrator > "%pdpath%\File permissions\change rights only_administrator_account.txt" 
@echo This file has change rights only for system > "%pdpath%\File permissions\change rights only_system_account.doc"
echo Y| cacls "%pdpath%\File permissions\change rights only_administrator_account.txt" /D system > nul 
echo Y| cacls "%pdpath%\File permissions\change rights only_system_account.doc" /D system > nul 
echo Y| cacls "%pdpath%\File permissions\change rights only_administrator_account.txt" /E /G administrator:c  > nul 
echo Y| cacls "%pdpath%\File permissions\change rights only_system_account.doc" /E /G system:c > nul 

rem pause

rem --------------
rem Dates on Files
rem --------------
set /A step=step+1
@echo Step %step%: Creating files with odd dates on them
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
rem old files
copy "%curpath%\bin\template file.txt " "%pdpath%\Dates on Files\old_file.txt" > nul
@echo This file has a date of 12-31-1970 > "%pdpath%\Dates on Files\old_file.txt"
"%curpath%\bin\touch" -c -t 197001010101.00 "%pdpath%\Dates on Files\old_file.txt"
"%curpath%\bin\touch" -m -t 197001010101.00 "%pdpath%\Dates on Files\old_file.txt"
"%curpath%\bin\touch" -a -t 197001010101.00 "%pdpath%\Dates on Files\old_file.txt"
rem future files
copy "%curpath%\bin\template file.txt " "%pdpath%\Dates on Files\future_file.txt" > nul
@echo This file has a date of 01-01-2037 > "%pdpath%\Dates on Files\future_file.txt"
"%curpath%\bin\touch" -c -t 203701010101.00 "%pdpath%\Dates on Files\future_file.txt"
"%curpath%\bin\touch" -m -t 203701010101.00 "%pdpath%\Dates on Files\future_file.txt"
"%curpath%\bin\touch" -a -t 203701010101.00 "%pdpath%\Dates on Files\future_file.txt"

rem -----------------------------------------------------------------------------------
rem Files with device names in them (preceeding with a space char due to OS restriction)
rem -----------------------------------------------------------------------------------
set /A step=step+1
@echo Step %step%: Creating files with device names in them [preceeding with a space char due to OS restriction]
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
cscript //Nologo "%curpath%\bin\create_device_filename.vbs" "%pdpath%\Files with device names\"
@echo All files in this folder have spaces in front of thier names > "%pdpath%\Files with device names\All files in this folder have spaces in front of thier names" 

rem ----------------------------------------------------------------------------------
rem Files with Streams (stream_file.txt:stream_1.txt to stream_file.txt:stream_255.txt)
rem ----------------------------------------------------------------------------------
set /A step=step+1
@echo Step %step%: Creating a file with many streams on it
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
cscript //Nologo "%curpath%\bin\stream_files.vbs" "%pdpath%\Files with streams\" 256

rem ---------------------------
rem Create ASCII files upto 255
rem ---------------------------
set /A step=step+1
@echo Step %step%: Creating files with ASCII chars 0 to 255
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
cd %pdpath%
rem mkdir "Files 0 to 255"
cscript //Nologo "%curpath%\bin\create_files_0_255.vbs" "%pdpath%\Files 0 to 255\"

rem ----------------------------------------
rem Create long file name of up to 260 chars
rem ----------------------------------------
set /A step=step+1
@echo Step %step%: Creating file with 259 char file name
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
cd "%pdpath%"
rem mkdir "Long file name"
cscript //Nologo  "%curpath%\bin\create_long_filename.vbs" "%pdpath%\Long file name\"

rem --------------------------------------------------
rem Folders and files with leading and trailing spaces
rem --------------------------------------------------
set /A step=step+1
@echo Step %step%: Creating folders and files with leading and trailing spaces
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.
mkdir "%pdpath%\ FolderWithLeadingSPACE"
echo "file with a leading space" > "%pdpath%\ FolderWithLeadingSPACE\ FileWithLeadingSPACE"
echo "file with a trailing space" > "%pdpath%\ FolderWithLeadingSPACE\FileWithTrailingSPACE "

rem the os seems to prevent the trailing space folder
rem mkdir "%pdpath%\FolderWithTrailingSPACE "
rem echo "file with a leading space" > "%pdpath%\FolderWithTrailingSPACE \ FileWithLeadingSPACE"
rem echo "file with a trailing space" > "%pdpath%\FolderWithTrailingSPACE \FileWithTrailingSPACE "
rem @echo.

REM 			if %noshare%. EQU 1. (
REM 			rem --------------------------------------------------
REM 			rem Folder shared 1000 times
REM 			rem --------------------------------------------------
REM 			set /A step=step+1
REM 			@echo Step %step%: Creating a folder that has been shared 1000 times
REM 			@echo "Step %step%" >> "%pdpath%\started.txt"
REM 			@echo.
REM 			for /l %%i in (1,1,1000) do net share 123456789012345678901234567890123456789012345678901234567890123456789012345_%%i=%pdpath%\Folder_Shared_1000_times /remark:"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" > nul
REM 			)
REM 
REM rem @echo.

rem --------------------
rem Checking the dataset
rem --------------------
set /A step=step+1
if %pd_forcepath%. EQU 1. (
@echo Step %step%: SKIPPING Checking of the dataset... forcepath: %forcepath%
@echo.
goto END
)
if %maxpath%. NEQ . (
@echo Step %step%: SKIPPING Checking of the dataset... maxpath: %maxpath%
@echo.
goto END
)

@echo Step %step%: Checking the dataset...
@echo "Step %step%" >> "%pdpath%\started.txt"
@echo.

rem check the windows version
ver | find /i "xp" > nul
if %errorlevel%. EQU 0. (
set pd_osversion=_xp
) else (
set pd_osversion=
)

subst /d b: > nul
subst b: %pdpath% > nul
cscript //Nologo "%curpath%\bin\deep_paths.vbs" b:\ display > "%pddrive%:\pd_%pddriveTYPE%_data_check.tmp"
%pddrive%:
cd "%pdpath%"
@echo on
"%curpath%\bin\sort" -o "%pdpath%\pd_%pddriveTYPE%_data_check.txt" -d "%pddrive%:\pd_%pddriveTYPE%_data_check.tmp" 
rem del /q "%pddrive%:\pd_%pddriveTYPE%_data_check.tmp"
subst /d b: > nul
@echo off

rem if %pd_osversion%. EQU xp. (
fc "%curpath%\bin\pd_%pddriveTYPE%_baseline_check%pd_osversion%.txt" %pdpath%\pd_%pddriveTYPE%_data_check.txt > nul
rem ) else (
rem fc .\bin\pd_%pddriveTYPE%_baseline_check.txt %pdpath%\pd_%pddriveTYPE%_data_check.txt > nul
rem )

if %errorlevel%. NEQ 0. (
@echo PROBLEMATIC DATASET DID NOT INSTALL PROPERLY!!!
@echo Run the following command from the command prompt to see the differances:
@echo "%pdpath%\bin\wdiff" %pdpath%\pd_%pddriveTYPE%_data_check.txt %pdpath%\bin\pd_%pddriveTYPE%_baseline_check%pd_osversion%.txt
REM start "%pdpath%\bin\wdiff" %pdpath%\pd_%pddriveTYPE%_data_check.txt %pdpath%\bin\pd_%pddriveTYPE%_baseline_check.txt
set /A globalerror=globalerror+1
goto error
) else (
@echo          The Dataset appears to be GOOD!!! 
)

GOTO END

:USAGEERROR
set /A globalerror=globalerror+1
@echo.
@echo File name: setup.bat
@echo. 
@echo Description: 
@echo.       This script copies and creates the problematic data set on the destination computer.
@echo.       This script MUST be run from a mapped drive as the current working directory.
@echo.       The destination parameter SHOULD be a LOCAL NTFS volume.
@echo.       If the destination parameter IS NOT a LOCAL NTFS volume some data types will be skipped (ie: installing to a filer drive)
@echo.
@echo Usage: 
@echo        setup.bat "Destination_DRIVE" (the destination folder will be [Destination_DRIVE]:\pd_dst)
@echo        EXAMPLE: setup.bat D:
@echo.
@echo.       To remove an existing dataset: use the EXACT path to delete as parameter 1, use the delete command as parameter 2. 
@echo.       EXAMPLE: setup.bat "D:\pd_dst" delete
@echo.
@echo.       To force installation to a given path use the EXACT path to install as parameter 1, use the forcepath command as parameter 2.
@echo.       When forcepath is used the data set will not be checked for correct installation.
@echo.       EXAMPLE: setup.bat "D:\some path" forcepath
@echo.
@echo.       To set the maximum path depth of the deep directory, set environment variable "maxpath" to a number value before running setup
@echo.       When maxpath is used the data set will not be checked for correct installation.
@echo.       EXAMPLE: 
@echo.       set maxpath=500
@echo.       setup.bat D:
@echo.

GOTO END

:ERROR
@echo Error occurred !!!

:END
@echo.
@echo.
@echo.

@echo %date% %time% PROBLEMATIC DATA SETUP FINISHED with error code[%globalerror%] >> "%curpath%\pd.log"
rem if running through automation exit right away, if not exit the bat and leave the shell open

if %pdexitshell%. EQU 1. (
exit %globalerror%
) else (
exit /B %globalerror%
)
