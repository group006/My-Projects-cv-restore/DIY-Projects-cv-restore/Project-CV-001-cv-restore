Dim fso, fileline

set objArgs = Wscript.Arguments

Const ForReading = 1
Const ForWriting = 2

Set fso = CreateObject("Scripting.FileSystemObject")
on error resume next

filename = objArgs(0) + "stream_file.txt"
numstreams  = objArgs(1)
Set thefile = fso.CreateTextFile( filename, ForWriting, False )
thefile.writeline filename
thefile.Close

for x = 0 to numstreams
	filename = objArgs(0) + "stream_file.txt" + ":stream_" + cstr(x) + ".txt"
	'wscript.echo filename 
	Set thefile = fso.CreateTextFile( filename, ForWriting, False )
	thefile.writeline "stream_" + cstr(x)
	thefile.Close
next
