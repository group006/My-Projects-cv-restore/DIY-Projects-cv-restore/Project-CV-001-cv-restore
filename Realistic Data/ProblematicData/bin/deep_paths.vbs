Dim objFSO

On Error Resume Next

set objArgs = Wscript.Arguments

Const ForReading = 1
Const ForWriting = 2

driveletter = left(objArgs(0), 1)
startpath = mid(objArgs(0), 3)
currentfolder = "\\?\" & driveletter & ":" & startpath
displayoffset = 5

If Wscript.Arguments.count = 4 Then
	If LCase(objArgs(3)) = "nt" Then
		currentfolder = driveletter & ":" & startpath
		displayoffset = 1
	End If
End If

if len(startpath) < 1 then wscript.quit -1

Set objFSO = CreateObject("Scripting.FileSystemObject")

Select Case lcase(objArgs(1))
	
	Case "add"

		x = 1	
		maxpathlen = objArgs(2)
		objFSO.CreateFolder (currentfolder)
		
		do while len(currentfolder) < maxpathlen 
			currentfolder = currentfolder & "\" & cstr(x)
			'wscript.echo currentfolder 
			objFSO.CreateFolder (currentfolder)
			'If Err.Number <> 0 Then Call Main_error()
			filename = currentfolder & "\test_file.txt"
			'wscript.echo filename 
			Set thefile = objFSO.CreateTextFile( filename, ForWriting, False )
			thefile.writeline (filename) 
			thefile.Close
			x = x + 1
			if cint(len(currentfolder)) > cint(maxpathlen) then
				wscript.echo ("max path reached!!")
				wscript.echo "Path_len: " & len(currentfolder), "MaxPath_len_requested: " &cint(maxpathlen)
				exit do
			end if
		loop
		wscript.quit 0
		
	Case "delete"
		'wscript.echo "Deleting: " & currentfolder
		objFSO.DeleteFolder (currentfolder)
		If Err.Number <> 0 Then Call Main_error()
		wscript.quit 0
	
	Case "display"
		Dim fso, f, s	
		createtime = False
		accesstime = False
		modtime = False
		
		If Wscript.Arguments.count = 3 Then
			If InStr(LCase(objArgs(2)), "c") > 0 Then createtime = True
			If InStr(LCase(objArgs(2)), "a") > 0 Then accesstime = True
			If InStr(LCase(objArgs(2)), "m") > 0 Then modtime = True								
		End If
		
		Set objFolder = objFSO.GetFolder(currentfolder)	
		WScript.Echo Mid(objFolder.Path, displayoffset)
		Set colFiles = objFolder.Files
		
		For Each objFile In colFiles
		  WScript.Echo Mid(ShowFileAccessInfo(objFile.Path, createtime, accesstime, modtime),displayoffset)
		Next
		
		ShowSubFolders(objFolder) 
		
		Sub ShowSubFolders(objFolder)
			
		  Set colFolders = objFolder.SubFolders	
		  For Each objSubFolder In colFolders	
		    WScript.Echo Mid(objSubFolder.Path, displayoffset)
		    Set colFiles = objSubFolder.Files
		
		    For Each objFile In colFiles
		    	WScript.Echo Mid(ShowFileAccessInfo(objFile.Path, createtime, accesstime, modtime),displayoffset)
		    Next
		
		    ShowSubFolders(objSubFolder)	
		  Next
		  	
		End Sub
		
	Case else
	
		wscript.echo "driveletter: " & driveletter
		wscript.echo "startpath: " & startpath
		wscript.echo "currentfolder: " & currentfolder
		wscript.quit -1
	
End Select

Set objFSO = Nothing
wscript.quit 0
	
Sub Main_error()

	wscript.echo ("[" & CStr(Err.Number) & "] [" & Err.description & "]")
	wscript.quit errorcode
	
End Sub

Function ShowFileAccessInfo(filespec, createtime, accesstime, modtime )
	 
		s=""
		'filespec = "\\?\" & filespec
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set f = fso.GetFile(filespec)
		s = f.Path 
		If createtime Then s = s & " [Ctime:" & f.DateCreated & "]"
		If accesstime Then s = s & " [Atime:" & f.DateLastAccessed & "]"
		If modtime Then s = s & " [Mtime:" & f.DateLastModified & "]"  
		ShowFileAccessInfo = LCase(s)
		Set fso = Nothing
   
End Function	