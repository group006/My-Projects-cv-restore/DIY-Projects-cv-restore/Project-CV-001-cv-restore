Dim fso, fileline

set objArgs = Wscript.Arguments

Const ForReading = 1
Const ForWriting = 2

Set fso = CreateObject("Scripting.FileSystemObject")

fileline = objArgs(0) 
If Right (fileline, 1) <> "\" Then fileline = fileline & "\"
limit = 260

on error resume next
for x = 1 to limit
	fileline = fileline & "1"	
	'wscript.echo fileline 
	Set thefile = fso.CreateTextFile( fileline, ForWriting, False )
	thefile.writeline (fileline)
	thefile.writeline "This filename is [" & cstr(len(fileline)) & "] characters long"
	thefile.Close
next

