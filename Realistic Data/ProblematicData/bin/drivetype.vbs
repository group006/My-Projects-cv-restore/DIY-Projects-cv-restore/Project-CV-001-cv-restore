set objArgs = Wscript.Arguments

If Wscript.Arguments.count = 0 Then 
	wscript.echo "Usage: cscript drivetype.vbs [driveletter]"
	wscript.quit 1
Else
	drvPath = Left(objArgs(0), 1)
End If

Dim fso, d
Set fso = CreateObject("Scripting.FileSystemObject")
Set d = fso.GetDrive(drvpath)

drivetype = ""
	  
Select Case d.DriveType
    Case 0: drivetype = "unknown"
    Case 1: drivetype = "removable"
    Case 2: drivetype = "fixed"
    Case 3: drivetype = "network"
    Case 4: drivetype = "cd-rom"
    Case 5: drivetype = "ram disk"
 End Select
 
wscript.echo drivetype