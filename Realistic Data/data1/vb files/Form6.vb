Public Class Form6

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim width As Integer = Integer.Parse(tbWidth.Text)
        Dim height As Integer = Integer.Parse(tbHeight.Text)
        Dim numBoxesVert As Integer = Integer.Parse(tbNumBoxesVert.Text)
        Dim numBoxesHoriz As Integer = Integer.Parse(tbNumBoxesHoriz.Text)

        ' Declare variables for our loop counters
        Dim w, h, hBox, vBox As Integer

        Label1.Text = ""

        For vBox = 1 To numBoxesVert
            For h = 1 To height
                For hBox = 1 To numBoxesHoriz
                    For w = 1 To width
                        ' Label1.Text = Label1.Text & "X"
                        Label1.Text = Label1.Text & (width * (h - 1) + w)
                    Next w
                    Label1.Text = Label1.Text & " "
                Next hBox
                Label1.Text = Label1.Text & vbCrLf
            Next h
            Label1.Text = Label1.Text & vbCrLf
        Next vBox
    End Sub
End Class