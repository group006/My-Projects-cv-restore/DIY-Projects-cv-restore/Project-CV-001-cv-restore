Public Class Form7

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Select Case TextBox1.Text.ToLower()
            Case "1", "jan", "january"
                MsgBox("January has 31 days")
            Case "2", "feb", "february"
                ' Check if February is a leap year
                MsgBox("February has 28 days")
            Case "3", "mar", "march"
                MsgBox("March has 31 days")
            Case "4", "apr", "april"
                MsgBox("April has 30 days")
            Case "5", "may" ' the abbreviation for May is the same as the full name of the month
                MsgBox("May has 31 days")
            Case "6", "jun", "june"
                MsgBox("June has 30 days")
            Case "7", "jul", "july"
                MsgBox("July has 31 days")
            Case "8", "aug", "august"
                MsgBox("August has 31 days")
            Case "9", "sep", "september"
                MsgBox("September has 30 days")
            Case "10", "oct", "october"
                MsgBox("October has 31 days")
            Case "11", "nov", "november"
                MsgBox("November has 30 days")
            Case "12", "dec", "december"
                MsgBox("December has 31 days")
            Case Else
                MsgBox("That is not a valid month")
        End Select
    End Sub
End Class