Module Module1


    Sub Main()
        Const maxNumbers As Integer = 100
        Dim numNumbers As Integer = 0
        Dim numbers(maxNumbers) As Double
        Dim s As String = "n"

        Do While numNumbers <= 100 And s <> "q"
            Console.Write("Enter a number or ""q"" to quit: ")
            s = Console.ReadLine()
            Console.WriteLine("s=" & s)
            If s <> "q" Then
                numbers(numNumbers) = Double.Parse(s)
                Console.WriteLine("double=" & numbers(numNumbers))
                numNumbers += 1
            End If
        Loop

        Dim i As Integer
        For i = 0 To numNumbers - 1
            Console.WriteLine(i.ToString() & " : " & numbers(i).ToString())
        Next

        Console.WriteLine("Calling DisplayNums")
        displayNums(numbers, numNumbers)

        Console.WriteLine("The largest number is " & largest(numbers, numNumbers))
        Console.ReadLine()

    End Sub



    Sub displayNums(ByVal nums() As Double, ByVal numElements As Integer)
        Dim i As Integer

        For i = 0 To numElements - 1
            Console.WriteLine(i.ToString() & " : " & nums(i))
        Next

    End Sub


    Function largest(ByVal nums() As Double, ByVal numElements As Integer) As Double

        Dim i As Integer

        largest = nums(0)
        For i = 0 To numElements - 1
            If nums(i) > largest Then
                largest = nums(i)
            End If
        Next

    End Function

End Module