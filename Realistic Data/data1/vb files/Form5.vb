Public Class Form5

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim startingPosition, sum As Integer
        Dim digit As String
 
        For startingPosition = 0 To TextBox1.Text.Length - 1

            digit = TextBox1.Text.Substring(startingPosition, 1)

            ' The following line is NOT necessary. It will display the digit 
            ' that we are up to. However, you can take this out and the program
            ' will still work.
            MessageBox.Show("digit: " & digit)

            sum = sum + Integer.Parse(digit)

        Next
        MessageBox.Show("The sum of the digits in the number " & _
                        TextBox1.Text & " is : " & sum)

    End Sub
End Class