Public Class Form3

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Button2.Text = "Goodbye!"
        TextBox1.Text = "Hope you"
        Label1.Text = "had fun."

        Button1.Text = "Text properties were changed."
    End Sub

    Private Sub Label1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Label1.Click
        MessageBox.Show("You clicked on Label1")
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        MessageBox.Show("You changed my text!")
    End Sub

End Class